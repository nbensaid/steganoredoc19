# StegoSploit For REDOC 2019, Thales

This package is a modified version of the StegoSploit system as published in the main paper.

The main links for this system are the following:
* Official page [stegosploit.info](https://stegosploit.info/)
* Presentation [BackHat](https://www.blackhat.com/docs/eu-15/materials/eu-15-Shah-Stegosploit-Exploit-Delivery-With-Steganography-And-Polyglots.pdf)
* Presentation Video [Stegosploit Part1](https://www.youtube.com/watch?v=O9vSSQIZPlI)

**A clear understanding of the Stegosploit paper or of the
  presentation is a requirement in the remaining of this document**

The original Readme is still present: [README.TXT](./README.TXT)

The system has been enhanced with the integration and the management described in the following sections

## Requirements

1. For JS image processing codes:
  * nodejs (tested with v12.12.0)
  * npm (tested with 6.11.3)
  * For extra dependencies, in the directory "./stego", run:
```bash
    npm install
```

2. For Image visualization, the system was tested using Firefox 69.0.2 (64 bits).

## Main Wrapper

The main wrapper program is [encodeImage.sh](./encodeImage.sh), it
embeds a code in an image with the proper HTML/JS decoder in the file.
```bash
    $ ./encodeImage.sh -h error: usage: ./encodeImage.sh <image>
    <output>
```

### The main steganographic parameters are set in the file :
    * L=3                                        # the image Layer
    * C=1                                        # the image channel (color, alpha)
    * G=10                                       # The grid offset
    * Q=0.95                                     # The compression/image quality of the generated image
    * CODE=${HOME_DIR}/exploits/firefox_popup.js # The code injected in the image

Creative common images can be found easily on internet. Example :
https://images.pexels.com/photos/371633/pexels-photo-371633.jpeg

### Main steps of the system
* Check the installation of NodeJS

* Build the HTML/javascript image decoder : The source code is in
[exploits/decoder_ext_script.js](./exploits/decoder_ext_script.js). The
file contains the image decoder and it is preprocessed this the global
parameters before injection in the image. The JS is inserted in the
HTML code with the template
[exploits/decoder_template.am](./exploits/decoder_template.am).

* The injection code is then steganogaphically injected in the image.
The application [stego/stego_encoding.js](./stego/stego_encoding.js)
is in NodeJS. The application inject the code using the global
parameters (layer, channel, grid), and the image is generated with the
provided quality.

### Encoding results : may fail

Depending on the injection code size, the image size, and the image
injection parameters, the result may fail. In that case, this kind of
output may be showed :

```bash
     (pyenv) [~/travail/Stegano/stegosploit]$ ./encodeImage.sh fresh_fruit_anyone__by_mrscarlet.jpg fresh.html
     Decoding Error: NaN > 462
     Decoding Error: NaN > 462
     Decoding Error: NaN > 462
     Decoding Error: NaN > 462
     Decoding Error: NaN > 462
     Error: could not encapsulate the code.
     error: during stegano
 ```

Steganographic parameters or the source image may need to be modified in order to successfully embed the image :
* Bigger source image
* Reduce the image layer (increase artefacts)
* Change the image quality (increase image size)
* ...

### Encoding results : in case of success

In case of success, the output of the generation is the following :

```bash
    (pyenv) [~/travail/Stegano/stegosploit]$ ./encodeImage.sh ./pexels-photo-371633.jpg ./out.html
	Decoding Error: NaN > 462
	Encoded text with 7 iterations: len=456
	The /tmp/img.23913.jpg file was saved!
	Final text: len=456
	./pexels-photo-371633.jpg
	Encapsulate in ./out.html
	
	                                  .       .
	                                 / `.   .' \
	                         .---.  <    > <    >  .---.
	                         |    \  \ - ~ ~ - /  /    |
	                          ~-..-~             ~-..-~
	                      \~~~\.'   stegosploit      `./~~~/
	                       \__/                imajs   \__/
	                         /  .    -.                 \
	                  \~~~\/   {              .-~ ~-.    -._ _._
	                   \__/    :        }     {       \     ~{  p'-._
	    .     .   |~~. .'       \       }      \       )      \  ~--'}
	    \\   //   `-..'       _ -\      |      _\      )__.-~~'='''''
	 `-._\\_//__..-'      .-~    |\.    }~~--~~  \=   / \
	  ``~---.._ _ _ . - ~        ) /=  /         /+  /   }
	                            /_/   /         {   } |  |
	                              `~---o.       `~___o.---'
	
	HTML header length = 5
	Pre Padding length = 624
	Post Padding length = 9717
	HTML data length = 1720
	Content length = 1728
	Final content length = 12074
	Written /tmp/st.23913
```

You can test that the system works using your browser: [out.html](./out.html)

Note that in order to make sure that the image will be interpreted as
an HTML code with a browser locally, the extension has been set to
".html", however, server properly configured would not need such an
extension.


## Helping pages.

A set of statically accessible web pages are available in order evaluate the steganographic approach:

* [Image analysis page](./stego/image_layer_analysis.html)
  + This page enable the analysis of the different image layers of a provided image.
* [Image decoder page](./stego/imagedecoder.html)
  + This page decode the message stored in an image, it requires the encoding parameters.
* [Image encoder page](./stego/iterative_encoding.html)
  + This page operate manually all the steganographic encoding process in the page.
  
This pages are provided for debugging and analysis purpose. On a recent browser, the
access control mechanism prevent the image processing system to create new images.
```javascript
    SecurityError: The operation is insecure. image_layer_analysis.html:209
        loadCanvas file://~/travail/Stegano/stegosploit/stego/image_layer_analysis.html:209
        recalcBitmask file://~./travail/Stegano/stegosploit/stego/image_layer_analysis.html:135
        imageLoaded file:///~/travail/Stegano/stegosploit/stego/image_layer_analysis.html:187
```

To disable this restriction, edit a temporary [firefox profile configuration](about:config) with the following flag:
security.fileuri.strict_origin_policy -> set to false

