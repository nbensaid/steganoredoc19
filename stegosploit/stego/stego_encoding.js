var fs = require('fs');
var Canvas = require('canvas');
var stegoLib  = require('./stego_lib');
var ArgumentParser = require('argparse').ArgumentParser;

function writeLog(logData) {
    console.log(logData[0]);
}
var stego = new stegoLib.StegoEncoder(writeLog, 30);

var argParser = new ArgumentParser({ version: '0.0.1', addHelp:true, description: 'Stegano-encapsuler' });
argParser.addArgument( [ '-e', '--encode' ],   { help: 'Encode', action: 'storeTrue' } );
argParser.addArgument( [ '-d', '--decode' ],   { help: 'decode', action: 'storeTrue' } );
argParser.addArgument( [ '-i', '--image' ],    { help: 'Source Image', action: 'store', required:true } );
argParser.addArgument( [ '-o', '--output' ],   { help: 'Output Image', action: 'store' } );
argParser.addArgument( [ '-C', '--code' ],     { help: 'Code to insert', action: 'store', defaultValue:'console.log("done");' } );
argParser.addArgument( [ '-f', '--codefile' ], { help: 'Filename of the code to insert', action: 'store' } );
argParser.addArgument( [ '-l', '--layer' ],    { help: 'Image layer (0-7)', action: 'store', defaultValue:'0', type: 'int' } );
argParser.addArgument( [ '-g', '--grid' ],     { help: 'Grid offset', action: 'store', defaultValue:'1', type: 'int' } );
argParser.addArgument( [ '-c', '--channel' ],  { help: 'Color channel (0-2 or 3)', action: 'store', defaultValue:'0', type: 'int' } );
argParser.addArgument( [ '-O', '--offset' ],   { help: 'Offset in the image', action: 'store', defaultValue:'0', type: 'int' } );
argParser.addArgument( [ '-q', '--quality' ],  { help: 'Image quality (jpeg)', action: 'store', defaultValue:'1', type: 'float' } );

var args = argParser.parseArgs();

if (args.output == null) {
    var extension = args.image.match(/\.([^\.]*)$/)[0];
    var name = args.image.substr(0, extension.length+1);
    args.output =  name + '_enc';
}

function decode(image, args) {
    var decodedText, stegoImage;
    [ decodedText, stegoImage ] = stego.decodeImage(args.image, image, args.quality, args.grid, args.channel, args.layer, args.offset, args.code.length+1);
    stego.clog("Final text: len="+decodedText.length);
    return decodedText;
}

function decodeImage(filename, args, cb) {
    Canvas.loadImage(filename).then((image) => { var dcode = decode(image, args); if (cb) cb(dcode); });
}

function checkImage(args, decodedText, stegoImage, canvas) {
    if (decodedText == args.code) {
        stego.saveImage(args.output, stegoImage, canvas, (genFile) => {
            decodeImage(genFile, args, (dcode) => {
                if (decodedText != dcode) {
                    console.log("Error: verifying the code inside the image...");
                    process.exit(1);
                }
            });
        });
    }
    else {
        console.log("Error: could not encapsulate the code.");
        process.exit(1);
    }
}

function encode(image, args) {
    stego.encodeImage(args.image, image,
                      args.code, args.quality,
                      args.grid, args.channel, args.layer, args.offset,
                      (done, iteration, decodedText, stegoImage, canvas) => {
                          checkImage(args, decodedText, stegoImage, canvas);
                      });
}

if (args.encode) {
    if (args.codefile != null) {
        fs.readFile(args.codefile, function (err, data) {
            if (err) { throw err; }
            args.code = data;
            //console.log("Load code:"+data);
            Canvas.loadImage(args.image).then((image) => { encode(image, args); });
        });
    }
    else Canvas.loadImage(args.image).then((image) => { encode(image, args); });
}
else if (args.decode) {
    decodeImage(args.image, args);
}
else argParser.printHelp();
