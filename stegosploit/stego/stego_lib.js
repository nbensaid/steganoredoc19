// tests if global scope is binded to window
var isBrowserVar=0;
var isBrowserFunc=new Function("try {return this===window;}catch(e){ return false;}");
function isBrowser() {
    if (!isBrowserVar) {
        if (isBrowserFunc()) isBrowserVar =  1;
        else                 isBrowserVar = -1;
    }
    return isBrowserVar == 1;
}

var Canvas = null;
if (!isBrowser()) {
    Canvas = require('canvas');
}

const ByteSize = 8;
const ImgBitSize = 4;
class ImageDesc {
    constructor(imageFile, width, height, quality, grid, encodingChannel, bitPosition, offset) {
        this.imageFile = imageFile;
        this.imgWidth = width;
        this.imgHeight = height;
        this.offset = offset;
        this.quality = quality;
        this.grid = ImageDesc.filterGrid(grid);
        this.encodingChannel = encodingChannel;
        this.bitPosition = ImageDesc.filterBitPosition(bitPosition);
        this.maxTextSize = 500;
        this.loadImageFile()
    }

    loadImageFile() {
        var extension = this.imageFile.toUpperCase().match(/\.([^\.]*)$/);
        if(extension && extension.length == 2) {
            this.fileType = extension[1];
        }
        /*
          if(this.fileType == "JPG" || this.fileType == "PNG") { clog("Image type: " + this.fileType); }
          else { clog("Unknown image type"); }
        */
    }

    getCoords(x, y) {
        return(((y * this.imgWidth) + x) * ImgBitSize);
    }

    getDataURL(canvas) {
        var result = "";
        if      (this.fileType == "JPG") { result = canvas.toDataURL("image/jpeg", { 'quality':this.quality, 'chromaSubsampling':1, 'progressive':1 }); }
        else if (this.fileType == "PNG") { result = canvas.toDataURL("image/png"); }
        return result;
    }

    getBase64(dataURL) {
        var result = dataURL;
        if      (this.fileType == "JPG") { result = result.replace("data:image/jpeg;base64,", ""); }
        else if (this.fileType == "PNG") { result = result.replace("data:image/png;base64,", ""); }
        return result;
    }

    static filterBitPosition(bitPosition) {
        if(bitPosition < 0) {
            bitPosition = 0;
        }
        if(bitPosition > 7) {
            bitPosition = 7;
        }
        return bitPosition;
    }

    static filterGrid(grid) {
        if(isNaN(grid) || grid <= 0) {
            grid  = 1;
        }
        return grid;
    }

    getBit(pix, n) {
        var redbit = (pix[n] & (1 << this.bitPosition)) >> this.bitPosition;
        var greenbit = (pix[n + 1] & (1 << this.bitPosition)) >> this.bitPosition;
        var bluebit = (pix[n + 2] & (1 << this.bitPosition)) >> this.bitPosition;

        var bit;

        switch(this.encodingChannel) {
        case 0: bit = redbit;   break; // red
        case 1: bit = greenbit; break; // green
        case 2: bit = bluebit;  break; // blue
        default:
            var mean = (redbit + greenbit + bluebit) / 3;
            bit = Math.round(mean);
            /*
            if(mean > 0 && mean < 1) {
              clog((n / 4) + "* " + bit + "|" + pix[n] + " " + pix[n + 1] + " " + pix[n + 2] + "|" + redbit + " " + greenbit + " " + bluebit);
            }
            */
        }
        return(String.fromCharCode(bit + 0x30));
    };

    buildImage(canvas) {
        var downloadImage = this.getDataURL(canvas);
        var rawBase64 = this.getBase64(downloadImage);
        var rawText = Buffer.from(rawBase64, 'base64')
        if (false) {
            var base64data = rawBase64.match(/.{1,72}/g).join("\n");
            //stego.clog("Result: "+base64data);
        }
        return rawText;
    }
}

class ImageCanvas {
    constructor(desc, image) {
        this.desc = desc;
        this.canvas = Canvas.createCanvas(desc.imgWidth, desc.imgHeight);
        this.loaded = false;
        this.count = 0;
        if (image) this.drawImage(image);
        else       this.context = null;
    }

    drawImage(image) {
        if (!this.context) { this.context = this.canvas.getContext('2d'); }
        this.context.drawImage(image, 0, 0);
        this.loaded = false;
        this.count+=1;
    }

    setImage(ic) {
        ic.getData();
        if (!this.context) { this.context = this.canvas.getContext('2d'); }
        this.context.putImageData(ic.image, 0, 0);
        this.loaded = false;
    }

    storeImage() {
        this.getData();
        this.context.putImageData(this.image, 0, 0);
        this.loaded = false;
    }

    reloadImage() {
        var _this = this;
        this.storeImage();
        this.context = null;
        var downloadImage = this.desc.getDataURL(this.canvas);
        var img = new Canvas.Image();
        img.onload = function(){
            _this.drawImage(img, 0, 0);
        }
        img.src = downloadImage;
    }

    getData() {
        if (!this.loaded) {
            if (!this.context) { this.context = this.canvas.getContext('2d'); }
            this.image = this.context.getImageData(0, 0, this.desc.imgWidth, this.desc.imgHeight);
            this.data  = this.image.data;
            this.loaded = true;
        }
        return this.data;
    }

    save(filename, cb) {
        if (isBrowser()) return false;

        const fs = require('fs');
        const fname = filename+'.'+this.desc.fileType.toLowerCase();
        var rawText = this.desc.buildImage(this.canvas);
        fs.writeFile(fname, rawText, function(err) {
            if (cb) cb(fname, err);
        });
    }
}

const ImgLenghtTypeSize =  6;
class StegoEncoder {
    constructor(log_cb, max_iter) {
        this.maxIter = max_iter;
        this.computeDelta = false;
        this.logData = new Array;
        this.writeLog_cb = log_cb;
    }

    clog(x) {
        this.logData.splice(0, 0, x);
        if (this.writeLog_cb) this.writeLog_cb(this.logData);
    }

    static loadCanvasData(embedText) {
        return StegoEncoder.stringToBinary(embedText);
    }

    static stringToBin(stringValue) {
        var size = stringValue.length;
        var result = "";
        for (var index=0; index < size; index++) {
            var bin = stringValue.charCodeAt(index).toString(2);
            var filled = bin;
            while (filled.length < ByteSize) filled = "0" + filled;
            result += filled;
        }
        return result;
    }

    static stringToBinary(stringValue) {
        var l = stringValue.length.toString();
        while (l.length < ImgLenghtTypeSize) l = "0" + l;
        return this.stringToBin(l + stringValue) ;
    }

    static binaryToString(binValue) {
        var returnString = "";
        for(var i = 0; i < binValue.length; i += ByteSize) {
            var x = binValue.substr(i, ByteSize);
            var b = String.fromCharCode(parseInt(x, 2));
            returnString += b;
        }
        return(returnString);
    }

    encodeData(imageDesc, pix, textData) {
        var bitstream = StegoEncoder.loadCanvasData(textData);

        // pix[i] : R, pix[i+1] : G, pix[i+2] : B, pix[i+3] : Alpha channel
        var i, r, g, b;
        var newR, newG, newB;
        var x = 0, y = 0;
        var y = Math.floor(imageDesc.offset / imageDesc.imgWidth);
        var x = imageDesc.offset % imageDesc.imgWidth;
        //this.clog("Encode with grid="+imageDesc.grid+ " x="+x+ " y="+y+" width="+imageDesc.imgWidth);

        for(var j = 0; j < bitstream.length; j++) {
            i = imageDesc.getCoords(x, y);

            r = pix[i];
            g = pix[i + 1];
            b = pix[i + 2];

            // override the least significant bit of RGB pixels
            // with our data bit depending upon which encoding
            // channel is chosen.
            if(j < bitstream.length) {
                var bit = bitstream.charCodeAt(j) - 0x30; // "0"
                bit = bit << imageDesc.bitPosition;
                var mask = 0xFF - (1 << imageDesc.bitPosition);

                newR = r;
                newG = g;
                newB = b;

                // encode based on the channel selected
                switch(imageDesc.encodingChannel) {
                case 0: newR = (r & mask) | bit; break; // red
                case 1: newG = (g & mask) | bit; break; // green
                case 2: newB = (b & mask) | bit; break; // blue
                default: // encode on all channels
                    newR = (r & mask) | bit;
                    newG = (g & mask) | bit;
                    newB = (b & mask) | bit;
                }

                pix[i] = newR;
                pix[i + 1] = newG;
                pix[i + 2] = newB;
            }
            else break;

            x += imageDesc.grid;
            if(x >= imageDesc.imgWidth) {
                x = 0;
                y += imageDesc.grid;
            }
            if(y >= imageDesc.imgHeight) { this.clog("Not enough space in image"); break; }
        }

        imageDesc.maxTextSize = bitstream.length / ByteSize;
        return pix;
    }

    sourceEncode(sourcePix, targetPix, deltaPix) {
        var differingPixels = 0;

        for(var i = 0; i < sourcePix.length; i += ImgBitSize) {
            var sr = sourcePix[i];
            var sg = sourcePix[i + 1];
            var sb = sourcePix[i + 2];
            var sa = sourcePix[i + 3];

            var tr = targetPix[i];
            var tg = targetPix[i + 1];
            var tb = targetPix[i + 2];
            var ta = targetPix[i + 3];

            var mean = Math.floor((sr + sg + sb) / 3);
            if(sr == tr && sb == tb && sg == tg && sa == ta) {
                // plot a B&W translucent pixel
                deltaPix[i] = mean;
                deltaPix[i + 1] = mean;
                deltaPix[i + 2] = mean;
                deltaPix[i + 3] = 64; // high transparency
            }
            else {
                // plot a red pixel
                deltaPix[i] = 255;
                deltaPix[i + 1] = 0;
                deltaPix[i + 2] = 0;
                deltaPix[i + 3] = 255;
                differingPixels++;
            }
        }
        return differingPixels;
    }

    readBitstream(imageDesc, pix, len, offset) {
        var a = [];
        var y = Math.floor(offset / imageDesc.imgWidth);
        var x = offset % imageDesc.imgWidth;
        //this.clog("Decode with grid="+imageDesc.grid+ " x="+x+ " y="+y+" width="+imageDesc.imgWidth);

        for (var p = 0, j = 0; j < len; j++) {
            var index = imageDesc.getCoords(x, y);
            a[p++] = imageDesc.getBit(pix, index);
            x += imageDesc.grid;
            if (x >= imageDesc.imgWidth) {
                x = 0;
                y += imageDesc.grid;
            }
        }
        return StegoEncoder.binaryToString(a.join(""));
    }

    decodePixels(imageDesc, pixelArray) {
        var lenOffset = ImgLenghtTypeSize * ByteSize * imageDesc.grid;
        var str = this.readBitstream(imageDesc, pixelArray, lenOffset, 0 + imageDesc.offset);
        var strLength = parseInt(str);

        // check if the length is valid, otherwise don't even bother to
        // decode the rest of the pixels.

        if(isNaN(strLength) || (strLength > imageDesc.maxTextSize)) {
            this.clog("Decoding Error: "+strLength+" > "+imageDesc.maxTextSize);
            return 0;
        }

        //this.clog("Length = " + strLength);
        str = this.readBitstream(imageDesc, pixelArray, strLength * ByteSize, lenOffset + imageDesc.offset);
        return str;
    }

    saveImage(filename, stegoImage, canvas, cb) {
        var _this = this;
        const fs = require('fs');
        const fname = filename+'.'+stegoImage.fileType.toLowerCase();
        var rawText = stegoImage.buildImage(canvas);
        fs.writeFile(fname, rawText, function(err) {
            if(err) { return _this.clog(err); }
            _this.clog("The "+fname+" file was saved!");
            if (cb) cb(fname);
        });
    }

    encodeImage(imageFile, image, code, quality, grid, encodingChannel, layer, offset, cb) {
        var stegoImage = new ImageDesc(imageFile, image.width, image.height,
                                       quality, grid, encodingChannel, layer, offset);

        var source = new ImageCanvas(stegoImage, image);	/* Src Image */
        var target = new ImageCanvas(stegoImage);		/* Target Image */
        target.setImage(source);

        if (this.computeDelta) {
            var delta = new ImageCanvas(stegoImage);
        }

        var done;
        var iteration = 1;
        var decodedText;
        var differingPixels = -1;
        do {
            done = true;
            this.encodeData(stegoImage, source.getData(), code);
            source.reloadImage();

            /* -- */
            if (this.computeDelta) {
                differingPixels = this.sourceEncode(source.getData(), target.getData(), delta.getData());
                var total = source.getData().length / ImgBitSize;
                var difference = differingPixels + "/" + total + " (" + ((differingPixels / total) * 100) + "%)";
                this.clog("Iteration " + iteration + " - delta " + difference);
                //delta.storeImage();
            }

            /* -- */
            target.setImage(source);
            decodedText = this.decodePixels(stegoImage, target.getData());

            if (this.computeDelta && (differingPixels == 0)) { this.clog("Abort"); }
            else {
                if (decodedText != code) {
                    //this.clog("Pass "+iteration+" iteration: @"+decodedText+"@<>@"+code+"@");
                    iteration++;
                    done = false;
                }
                else this.clog("Encoded text with "+iteration+" iterations: len="+decodedText.length);
            }
        }
        while (!done && iteration <= this.maxIter);
        if (cb) cb(done, iteration, decodedText, stegoImage, target.canvas);
    }

    decodeImage(imageFile, image, quality, grid, encodingChannel, layer, offset, maxSize) {
        var stegoImage = new ImageDesc(imageFile, image.width, image.height,
                                       quality, grid, encodingChannel, layer, offset);
        stegoImage.maxTextSize = maxSize;

        var source = new ImageCanvas(stegoImage, image);
        var decodedText = this.decodePixels(stegoImage, source.getData());

        return [ decodedText, stegoImage ];
    }
}

if (!isBrowser()) {
    module.exports = { ImageDesc, ImageCanvas, StegoEncoder };
}
