var stego;
var stegoImage = null;

var img;
var canvasSource;
var canvasTarget;
var canvasDelta;
var contextTarget;

//var encodingChannel = 0;   // R = 0, G = 1, B = 2, All = 3
//var bitPosition = 0;
//var quality;

var exploits;
var timeoutInterval = 500;

var sourcePix;
var iteration = 0;
var timer;

var histogram;
var listenerAdded = false;

var writeLog_cb = null;
var logData = new Array;

function clog(x) {
    logData.splice(0, 0, x);
    if (writeLog_cb) writeLog_cb(logData);
}

function writeLog(logData) {
    var log = document.getElementById("log");
    log.value = logData.join("\n");
}

function addExpCode(elist, code, desc) {
    elist[elist.length] = {};
    elist[elist.length-1].code = code;
    elist[elist.length-1].desc = desc;
}

function loadExp() {
    var all_basic = "toto='My Message'; console.log(toto); "
    var elist = new Array;
    addExpCode(elist, "", "--- choose an exploit ---");
    addExpCode(elist, ie_cinput_canvas, "IE CInput Use-After-Free calc");
    addExpCode(elist, firefox_cinput_canvas, "Firefox Reaper");
    addExpCode(elist, firefox_popup, "Firefox popup");
    addExpCode(elist, all_basic, "Basic");
    return elist;
}

function init() {
    writeLog_cb = writeLog;
    img = document.getElementById("original");
    canvasSource = document.getElementById("source"),
    canvasTarget = document.getElementById("target"),
    canvasDelta = document.getElementById("delta"),
    histogram = document.getElementById("histogram"),
    exploits = document.getElementById("exploits");

    canvasSource.height = canvasTarget.height = canvasDelta.height = img.height;
    canvasSource.width = canvasTarget.width = canvasDelta.width = img.width;

    stego = new StegoEncoder(writeLog);
    
    var elist = loadExp();
    for(var i = 0; i < elist.length; i++) {
        var expE = document.createElement("option");
        expE.value = elist[i].code;
        expE.innerHTML = elist[i].desc;
        exploits.appendChild(expE);
    }

    exploits.addEventListener("change", selectExploit);
    img.onload = imageLoaded;
    loadImage();
}

function selectExploit() {
    var code = exploits.options[exploits.selectedIndex].value;
    document.getElementById("embedtext").value = code;
}

function loadImage(imageFile) {
    var imageFile = document.getElementById("imageFile").value;
    img.src = imageFile + "?" + Math.random().toString();
}

function imageLoaded() {
    var resolution = document.getElementById("resolution")
    resolution.value = img.width + "x" + img.height;
    document.getElementById("process").disabled = false;
}

function toggleSlowMotion() {
    if(document.getElementById("slowmotion").checked) {
        timeoutInterval = 1000;
    }
    else {
        timeoutInterval = 500;
    }
}

function readEncodingChannel() {
    var v;
    var channels = document.getElementsByName("channels");
    for (var i = 0; i < channels.length; i++) {
        if(channels[i].checked) { v = channels[i].value; break; }
    }
    var result = 3;
    switch(v) {
    case "R": result = 0; break;
    case "G": result = 1; break;
    case "B": result = 2; break;
    }
    return result;
}

function getTextData() {
    var embedText = document.getElementById("embedtext").value;
    writeMD5(embedText, "md5input");
    return(embedText);
}

function writeMD5(str, element) {
    document.getElementById(element).value = md5(str);
}

/* ------------------------------------------------------- */

function iterate() {
    canvasTarget.width = img.width;
    canvasTarget.height = img.height;
    contextTarget = canvasTarget.getContext('2d');

    if(!listenerAdded) {
        img.addEventListener("load", afterSourceReplaced);
        listenerAdded = true;
        clog("iterate eventlistener added");
    }
    if (stegoImage) {
        img.src = stegoImage.getDataURL(canvasSource);
    }        
    iteration++;
}

function loadCanvas() {
    var grid = parseInt(document.getElementById("grid").value);
    var imageFile = document.getElementById("imageFile").value;
    var quality = parseFloat(document.getElementById("quality").value);
    var bitPosition = parseInt(document.getElementById("position").value);
    var encodingChannel = readEncodingChannel();

    clog("layer = "+bitPosition+", Grid = " + grid + ", quality = " + quality + ", channel = " + encodingChannel);

    canvasSource.width = img.width;
    canvasSource.height = img.height;
    var contextSource = canvasSource.getContext('2d');
    
    // draw the image onto the canvas
    contextSource.drawImage(img, 0, 0);

    var sourceImage = contextSource.getImageData(0, 0, canvasSource.width, canvasSource.height);
    sourcePix = sourceImage.data;

    stegoImage = new ImageDesc(imageFile, img.width, img.height, quality, grid, encodingChannel, bitPosition, 0);
    sourceImage.data = stego.encodeData(stegoImage, sourcePix, getTextData());

    sourceImage.data = sourcePix;
    contextSource.putImageData(sourceImage, 0, 0);
    document.getElementById("iterate").disabled = false;
    document.getElementById("stop").disabled = false;
    drawHistogram(canvasSource, histogram);
}

function writeDifference(count, total) {
    var difference = document.getElementById("difference");
    difference.value = count + "/" + total + " (" + ((count / total) * 100) + "%)";
    var iterationCount = document.getElementById("iteration");
    iterationCount.value = iteration;
    clog("Iteration " + iteration + " - delta " + difference.value);
}

function stopIterating() {
    clearTimeout(timer);
}

function afterSourceReplaced() {
    // for now, stop iterating
    clearTimeout(timer);
    // draw the image onto the canvas
    contextTarget.drawImage(img, 0, 0);

    var targetImage = contextTarget.getImageData(0, 0, canvasTarget.width, canvasTarget.height);
    var targetPix = targetImage.data;

    contextTarget.putImageData(targetImage, 0, 0);

    // create the delta canvas
    // loop through the source and target pixel data
    // and plot the differences on the delta canvas

    canvasDelta.width = img.width;
    canvasDelta.height = img.height;
    var contextDelta = canvasDelta.getContext('2d');

    var deltaImage = contextDelta.getImageData(0, 0, canvasDelta.width, canvasDelta.height);
    var deltaPix = deltaImage.data;

    var differingPixels = stego.sourceEncode(sourcePix, targetPix, deltaPix);
    var decodedText = stego.decodePixels(stegoImage, targetPix);

    contextDelta.putImageData(deltaImage, 0, 0);
    writeDifference(differingPixels, sourcePix.length / 4);
    document.getElementById("iterate").disabled = false;

    document.getElementById("decodedText").value = decodedText;
    writeMD5(decodedText, "md5output");

    if(decodedText != getTextData()) {
        loadCanvas();
        timer = setTimeout(iterate, timeoutInterval);
    }
    else {
        canvasTarget.style.border = "3px solid red";
        populateDownloadLink();
    }
}

function populateDownloadLink() {
    var downloadImage = stegoImage.getDataURL(canvasSource);
    var rawBase64 = stegoImage.getBase64(downloadImage);

    var base64data = rawBase64.match(/.{1,72}/g).join("\n");
    document.getElementById("base64").value = base64data;

    var rawText = Base64.decode(rawBase64);
    var endComment = rawText.indexOf("*/");
    if(endComment > 0) {
        clog("End Comment */ occurs at position " + endComment);
    }
    else {
        clog(fileType + " is clean. No end comment */ characters found");
    }
    download("stego", stegoImage.fileType, downloadImage);
}

function download(filename, extension, data) {
    var dl = document.createElement('a');
    dl.setAttribute('href', data);
    dl.setAttribute('target', '_blank');
    dl.setAttribute('download', filename + "." + extension);
    dl.style.display = "none";
    document.body.appendChild(dl);
    dl.click();
    document.body.removeChild(dl);
}

function saveImage() {
    loadCanvas();
    img.style.border = "3px solid red";
    canvasTarget.style.border = "0px";
}

