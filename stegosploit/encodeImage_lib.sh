#!/bin/bash

test x"${HOME_DIR}" == x"" && HOME_DIR=./

STEGO_HOME_JS=stego/
STEGO_JS=${STEGO_HOME_JS}/stego_encoding.js

DEC_TEMPLATE=exploits/decoder_template.am
DEC_SCRIPT=exploits/decoder_ext_script.js

IMAJS_HOME=imajs/
IMAJS_PNG=./html_in_png.pl
IMAJS_JPG=./html_in_jpg_ff.pl

TO_REMOVE=""
addTMP() {
    TO_REMOVE="${TO_REMOVE} $*"
}
cleanTmpFiles() {
    test x"${TO_REMOVE}" != x"" && rm -f ${TO_REMOVE}
}

log() {
    echo "$*"
}

error() {
    echo "error: $*"
    cleanTmpFiles
    exit 1
}

checkHomeDir() {
    test -e "${HOME_DIR}/${STEGO_JS}" && return 0
    local execf="$0"
    local bindir=$(dirname ${execf} )
    if test -e "${bindir}/${STEGO_JS}"; then
        HOME_DIR=${bindir}
    else
        error "Installation directory not found"
    fi
}

checkNodeJs() {
    checkHomeDir
    if pushd "${HOME_DIR}/${STEGO_HOME_JS}" > /dev/null || error "directory ${HOME_DIR}/${STEGO_HOME_JS} not found"; then
        if ! test -d node_modules; then
            npm install 2> /dev/null || error "problem installing external dependencies (node.js)"
        fi
        popd > /dev/null
    fi
}

buildDecoder() {
    test $# -ne 4 && error "missing parameters"
    local OUT_FILE=$1
    local LAYER=$2
    local CHANNEL=$3
    local GRID=$4

    local script=$(cat ${HOME_DIR}/${DEC_SCRIPT})
    script=${script/@LAYER@/${LAYER}}
    script=${script/@CHANNEL@/${CHANNEL}}
    script=${script/@GRID@/${GRID}}

    local template=$(cat ${HOME_DIR}/${DEC_TEMPLATE})
    echo "${template/@DEC_SCRIPT@/${script}}" > ${OUT_FILE}
}

embedHtmlImage() {
    test $# -ne 4 && error "missing parameters"
    local IMG_FILE=$1
    local STIMG_FILE=$2
    local HTML_FILE=$3
    local OUT_FILE=$4

    ST_FILE="/tmp/st.$$"
    buildDecoder ${TPL} ${L} ${C} ${G}

    if pushd "${HOME_DIR}/${IMAJS_HOME}" > /dev/null || error "directory ${HOME_DIR}/${IMAJS_HOME} not found"; then

        echo "$IMG_FILE"
        local file=$(basename -- "$IMG_FILE")
        local bfile="${file##*.}"
        local ext="${bfile##*.}"

        if test "${ext,,}" == "png"; then
            echo "Encapsulate in ${OUT_FILE}"
            perl ${IMAJS_PNG} "${HTML_FILE}" "${STIMG_FILE}.png" "${ST_FILE}" || error "during encapsulation"

        elif test "${ext,,}" == "jpg" -o "${EXT,,}" == "jpeg"; then
            echo "Encapsulate in ${OUT_FILE}"
            perl ${IMAJS_JPG} "${HTML_FILE}" "${STIMG_FILE}.jpg" "${ST_FILE}" || error "during encapsulation"

        else
            error "File format not supported: ${ext,,}"
        fi
        popd > /dev/null
    fi

    test -e "${ST_FILE}" && mv "${ST_FILE}" "${OUT_FILE}"
}
