# Sujet Thales REDOCS’19 #

## La stéganographie appliquée à la dissimulation de code malveillant ##

Les techniques stéganographiques sont employées dans de nombreux
domaines, et notamment dans le domaine de la dissimulation
d’information sensible ou de la protection de la propriété
intellectuelle. Cette technologie consiste à intégrer dans un flux
d’information légitime – une image, une vidéo, une archive, ou tout
autre médium – un message dont la présence ne doit pas être soupçonnée
et encore moins intelligible. Plus récemment et pour un usage plus
conventionnel, ces techniques peuvent être employées afin de
dissimuler des codes malveillants et ainsi tromper les technologies
d’analyse statique des anti-virus.

## Stéganographie et technologies de malware sur plateforme WEB ##

### Le principe appliqué aux navigateurs ###

Les modèles d’attaque que nous désirons étudier dans ce projet est
l’utilisation de malwares dans le but d’exfiltrer des informations
sensibles ou à provoquer une escalade de privilège (affectant
l’intégrité du système) en utilisant les vulnérabilités des
navigateurs. L’étude de ces vulnérabilités est en dehors du périmètre
de ce projet, mais les techniques stéganographiques [1,3] employées
dans ce domaine cherchent à cacher ces exploits au sein de pages web
ou de fichiers multimédias d’apparence anodine.

### Exemple de mise en oeuvre ###

La technique d’attaque par stéganographie [2] utilise typiquement
l’approche suivante :

Une page web intègre un code (type javascript) qui demande le
chargement d’une image, image vue et analysée comme une image conforme
par tout programme de lecture ou de galerie d’image, le code de
déclenchement doit sembler légitime et ne pas intégrer d’éléments
composant un malware, la complexité du décodage doit être modeste, les
capacités de calcul de la cible étant inconnues, l’empreinte mémoire
et CPU du décodeur doivent passer inaperçu.

### Exploitation avec des technologies d'I.A. ###

Objectif du projet, et mise en œuvre par les nouvelles technologies
disponibles

Au regard de l’analyse de l’état de l’art, l’objectif du projet est
d’évaluer la faisabilité d’une attaque par malware masquée par une
stéganographie à base de machine learning.

La potentialité d’une telle attaque est de plus en plus forte depuis
l’apparition de bibliothèques de deep-learning utilisant les capacités
de calcul numériques des GPU présents et accessibles pour les
navigateurs en utilisant les extensions WebGL 2.0 [4]. Cette
technologie est aujourd’hui disponible avec la plateforme
TensorFlow.js [5] qui propose apparemment l’ensemble des primitives
nécessaire à la mise en œuvre de l’approche : création des tenseurs,
décodage de l’image.

## Objectif : évaluer les DNN pour la stéganographie

Le principal résultat attendu serait de fournir les arguments
scientifiques qui nous permettraient de décider s’il est pertinent de
mettre en œuvre cette méthode et de développer les contres mesures
adéquates. L’évaluation du risque pourra se faire par différentes
approches scientifiques, par exemple :
  * l’analyse des éventuelles limites techniques à l’usage des
    techniques WebGL pour le machine-learning dans le cas des malwares
    – en intégrant les éventuelles évolutions technologiques,
  * l’évaluation mathématique de la complexité numérique en intégrant
    les simplifications potentielles qui garderaient acceptable le
    niveau de robustesse,

## Support Technique

Un _repository_ contenant les outils de base est fourni afin de
réduire le temps démarrage du projet. Ce dernier contient :

* Une version améliorée de [stegosploit](./stegosploit)

Cet ensemble d'outil a été extrait et documenté pour servir de base à
la mise en oeuvre des techniques stéganographiques très basiques dans
des images et son injection dans une image chargée par une page web.
Pour avoir plus de détails, consulter la petite
[documentation](./stegosploit/REDOC.md)

* Un lien vers le _repository_ de [Steganogan](https://github.com/DAI-Lab/SteganoGAN)

Cet outil illustre la mise en oeuvre de techniques steganographiques à
partir de techniques d'apprentissage. La documentation en ligne est complète.

* Les principaux outils susceptibles de porter l'architecture de DNN
  de Steganogan sur navigateur web sont présentés dans le document
  [SteganoGANandTFjs.md](./SteganoGANandTFjs.md)

## Références

[1] K. A. Zhang, A. Cuesta-Infante, and K. Veeramachaneni. Steganogan:
Pushing the limits of image steganography, arXiv preprint
arXiv:1901.03892, 2019. https://arxiv.org/abs/1901.03892

[2] Saumil Sha, Exploit Delivery via Steganography and Polyglots,
http://stegosploit.info/

[3] Ye, J., Ni, J., and Yi, Y. Deep learning hierarchical
representations for image steganalysis. IEEE Trans. On Information
Forensics and Security, 12(11):2545–2557, Nov 2017. ISSN
1556-6013. doi: 10.1109/TIFS.2017. 2710946

[4] https://www.khronos.org/registry/webgl/specs/latest/2.0/

[5] D. Smilkov and all. Tensorflow.js: Machine Learning For The Web
and Beyond. SysML Conférence, Palo Alto, CA, USA, 2019. URL:
https://www.tensorflow.org/js/
