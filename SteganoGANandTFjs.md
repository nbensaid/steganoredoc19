# SteganoGan over TensorFlow.JS, a technical overview #

This document provides an overview of the technologies available for
the deployment of Deep Learning on Python, Node.Js, and Javascript for
modern browser. The purpose is to identify the critical elements
needed for the port of the SteganoGAN [1][2] algorithm on a web browser.

A first section describes the Torch neural network implementation in
SteganoGAN. A second section provides an introduction of the target
DNN framework: PyTorch, TensorFlow.js, and the available tools to either
perform the learning phase using the Javascript model, or operate a
direct conversion of the existing model available with SteganoGAN.

## SteganoGAN and DNN  ##

The Deep Leaning system of the SteganoGAN framework is based on the
Python Torch library. It uses the basic features available in the NN
library to model, train, and operate on steganographic images.

### Models, Neural network architecture and weight ###

The SteganoGAN tool proposes three models 'basic', 'residual' and
'dense'. Each model is stored in a dedicated file. Each file contains
a serialized description of the model in a _'pytorch'_ format together
with the _'weight'_ data structures. The mechanism is based on
_'pickle'_ and described in the
[manual](https://pytorch.org/tutorials/beginner/saving_loading_models.html)

SteganoGAN models can be found in the directory
[steganogan/pretrained](./SteganoGAN/pretrained/).

### SteganoGAN Models ###

SteganoGAN model source codes are stored in the files
[steganogan/encoder.py](./SteganoGAN/encoders.py) and
[steganogan/decoders.py](./SteganoGAN/decoders.py). These
files describe the models used for the implementation of the
generation of two steganographic models: _'basic'_ and _'dense'_. Each
model is described by class and a _'build_models'_
function.

* Following, the _basic_ model example:

```python
    def _conv2d(self, in_channels, out_channels):
        return nn.Conv2d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=3,
            padding=1
        )

    def _build_models(self):
        self.layers = nn.Sequential(
            self._conv2d(3, self.hidden_size),
            nn.LeakyReLU(inplace=True),
            nn.BatchNorm2d(self.hidden_size),

            self._conv2d(self.hidden_size, self.hidden_size),
            nn.LeakyReLU(inplace=True),
            nn.BatchNorm2d(self.hidden_size),

            self._conv2d(self.hidden_size, self.hidden_size),
            nn.LeakyReLU(inplace=True),
            nn.BatchNorm2d(self.hidden_size),

            self._conv2d(self.hidden_size, self.data_depth)
        )

        return [self.layers]

```

Neural networks layers are composed of predefined _'Torch'_ modules:
_'LeakyReLU'_, _'BatchNorm2d'_, built over a parameterized _conv2d''_.

### SteganoGAN learning phase  ###

The proposed SteganoGAN implementation provides the necessary links to
build again the steganographic models that are operated by the
decoder. The tool set is available in the directory
[SteganoGAN/research/](./SteganoGAN/research/). The
original data set can be recovered in 'data' directory.

## Pytorch, TensorFlow, and TensorFlow.JS: some links  ##

The purpose of Pytorch and TensorFlow are the same: provide a fast and
comprehensive framework for operating DNN.

The open-source _Pytorch_ [3] library is operated using the former
_Caffe2_ [4] library DNN framework and is able to exploit optimized
CPU and GPU compute libraries. A strong documentation [6] is available
for the installation, the development, and the optimization of Torch
NN.  A specialized sub-project proposes an optimized extension for
computer vision applications [7]. The project is now supported by the
company Faceb**k.

The open-source _TensorFlow_ [8] library is very similar to the
_Pytorch_ library, it provides the same level of support [9][10], but
it is supported by G**gle. It was originally operated in C++ with a
Python interface but now, build-in in the framework, a
Javascript/NodeJS interface is available [11]. The _TensorFlow.js_
interface is available directly on browsers [12][13].

### From _pytorch_ to _TensorFlow_ and _TensorFlow.js_ ###

The existing pytorch models does not necessarily need to be trained
again with the TensorFlow.JS framework. Based on the
[Onnx standardization efforts](https://onnx.ai/about) to provide a
common theoretical and technical framework for the definition of
unique DNN syntax, several tools and methods have been developed to
convert Torch models into TensorFlow models.

An example is provided in the blog
[_Converting a Simple Deep Learning Model from PyTorch to TensorFlow_]](https://makerspace.aisingapore.org/2019/05/converting-a-simple-deep-learning-model-from-pytorch-to-tensorflow/).
[14]. Based on the SteganoGAN saved model in Pytorch format [15], a
conversion to this pivot format, and then to the TensorFlow format is
possible.

The conversion from TensorFlow to TensorFlow.js needs to be done in a
second step. For this purpose, the TensorFlow framework provides all
the necessary tools but they need to be operated in a Node.js or a
Python environment [16]. One of them is operated using the effective Kera
library [17] [18]. The process is described in the TensorFlow core
documentation [19].

# References #

[1] [SteganoGAN Paper](https://arxiv.org/abs/1901.03892)

[2] [SteganoGAN Github](https://github.com/DAI-Lab/SteganoGAN)

[3] [PyTorch](https://pytorch.org/)

[4] [Caffe2](https://caffe2.ai/)

[6] [PyTorch documentation](https://pytorch.org/docs/stable/index.html)

[7] [torchvision](https://pytorch.org/docs/stable/torchvision/index.html)

[8] [Why TensorFlow](https://www.tensorflow.org/)

[9] [TensorFlow Core r2.0 API documentation](https://www.tensorflow.org/api_docs/python/tf)

[10] [TensorFlow Graphics](https://www.tensorflow.org/graphics)

[11] [TensorFlow.js is a library for machine learning in JavaScript](https://www.tensorflow.org/js/?hl=en)

[12] [TensorFlow.js, training & deploying machine learning models in browsers](https://www.tensorflow.org/js/tutorials)

[13] [Add TensorFlow.js in browsers](https://www.tensorflow.org/js/tutorials/setup#usage_via_script_tag)

[14] [Converting a Simple Deep Learning Model from PyTorch to TensorFlow](https://makerspace.aisingapore.org/2019/05/converting-a-simple-deep-learning-model-from-pytorch-to-tensorflow/).

[15] [PyTorch: Saving and Loading Models](https://pytorch.org/tutorials/beginner/saving_loading_models.html)

[16] [TensorFlow Conversions](https://www.tensorflow.org/js/guide/conversion)

[17] [Keras: The Python Deep Learning library](https://keras.io/)

[18] [https://keras.io/getting-started/faq/#how-can-i-save-a-keras-model](https://keras.io/getting-started/faq/#savingloading-whole-models-architecture-weights-optimizer-state)

[19] [TensorFlow Using the SavedModel format](https://www.tensorflow.org/guide/saved_model)
